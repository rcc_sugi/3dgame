#include"DxLib.h"
#include"PlayerData.h"

CPlayerData::CPlayerData()
{
	DrawFormatString(0,80,GetColor(0,255,0),"playerdata_complete");
	ScreenFlip();

	PHandle =0;
	MV1SetScale(PHandle,VGet(0,0,0));

	HPMax=0;
	StaminaMax=0;
	Stealth=0;
	Silence=0;
	RecoilControl=0;
	Weight=0;
	Width=0;

}


void CPlayerData::MikuInit()
{

	PHandle = MV1LoadModel( "dat/Latฎ~N/Latฎ~NVer2.3_Normal.mv1" ) ;
	MV1SetScale(PHandle,VGet(0.09f,0.09f,0.09f));

	HPMax=1000;
	StaminaMax=1000;
	Stamina=StaminaMax;
	Stealth=1000;
	Silence=1000;
	RecoilControl=1000;

	StandHeight=1.6f;	CrouchHeight=1.1f;	CreepHeight=0.4f;
	Height=StandHeight;
	Weight=42;
	Width=4;
	StandSpeed=0.1f;
	CrouchSpeed=0.08f;
	CreepSpeed=0.05f;
	OnGroundFlag=0;
}
void CPlayerData::ReimuInit()
{
	PHandle=MV1LoadModel("dat/ํ์ฒX/reimuX_typeB.mv1");
	MV1SetScale(PHandle,VGet(0.09f,0.09f,0.09f));

	HPMax=1000;
	StaminaMax=1000;
	Stamina=StaminaMax;
	Stealth=1000;
	Silence=1000;
	RecoilControl=1000;

	StandHeight=1.6f;	CrouchHeight=1.1f;	CreepHeight=0.4f;
	Height=StandHeight;
	Weight=42;
	Width=4;
	StandSpeed=0.1f;
	CrouchSpeed=0.08f;
	CreepSpeed=0.05f;
	OnGroundFlag=0;

}

void CPlayerData::MohikanInit()
{
	PHandle=MV1LoadModel("dat/KLฌ1.0Zbg/qbn[1.0/qbn[PDOเ.mv1");
	MV1SetScale(PHandle,VGet(0.09f,0.09f,0.09f));

	HPMax=1000;
	StaminaMax=1000;
	Stamina=StaminaMax;
	Stealth=1000;
	Silence=1000;
	RecoilControl=1000;

	StandHeight=1.6f;	CrouchHeight=1.1f;	CreepHeight=0.4f;
	Height=StandHeight;
	Weight=42;
	Width=4;
	StandSpeed=0.1f;
	CrouchSpeed=0.08f;
	CreepSpeed=0.05f;
	OnGroundFlag=0;

}
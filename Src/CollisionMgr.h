#ifndef COLLISIONMGR
#define COLLISIONMGR

#define PLAYER_MAX_HITCOLL			512		// 処理するコリジョンポリゴンの最大数
#define PLAYER_ENUM_DEFAULT_SIZE		3.2f		// 周囲のポリゴン検出に使用する球の初期サイズ
#define PLAYER_HIT_TRYNUM			16		// 壁押し出し処理の最大試行回数
#define PLAYER_HIT_WIDTH			0.6f		// 当たり判定カプセルの半径
#define PLAYER_HIT_HEIGHT			2.0f		// 当たり判定カプセルの高さ
#define PLAYER_HIT_SLIDE_LENGTH			0.1f		// 一度の壁押し出し処理でスライドさせる距離

#include"CharaMgr.h"
#include"Stage.h"

class CCollisionMgr{

public:

	CCollisionMgr();
	~CCollisionMgr();

	void Process(CCharaMgr*,CStage*);
	void Init();

	unsigned int HitFlag;
	int DrawDataFlag,DataTime;

	MV1_COLL_RESULT_POLY_DIM HitDim ;			// プレイヤーの周囲にあるポリゴンを検出した結果が代入される当たり判定結果構造体
	int KabeNum,YukaNum;

	int i,k,j,Count;
	MV1_COLL_RESULT_POLY *Kabe[ PLAYER_MAX_HITCOLL ] ;	// 壁ポリゴンと判断されたポリゴンの構造体のアドレスを保存しておくためのポインタ配列
	MV1_COLL_RESULT_POLY *Yuka[ PLAYER_MAX_HITCOLL ] ;	// 床ポリゴンと判断されたポリゴンの構造体のアドレスを保存しておくためのポインタ配列
	MV1_COLL_RESULT_POLY *Poly ;				// ポリゴンの構造体にアクセスするために使用するポインタ( 使わなくても済ませられますがプログラムが長くなるので・・・ )
	HITRESULT_LINE LineRes ;				// 線分とポリゴンとの当たり判定の結果を代入する構造体

	float MaxY,oldMaxY;
	float Gravity,FallSpeed;
};

#endif
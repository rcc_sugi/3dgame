#include"DxLib.h"
#include"CameraMgr.h"
#include<math.h>

CCameraMgr::CCameraMgr(){
	DrawFormatString(0,140,GetColor(0,255,0),"camera_complete");
	ScreenFlip();

    //奥行0.1〜1000までをカメラの描画範囲とする
    SetCameraNearFar( 0.1f, 2000.0f ) ;
	CameraSwitch_=RIGHT;
	AngleH=0.f;
	AngleV=DX_PI_F/12;
	CameraDistance=4.f;
	CameraSpeed=0.04f;
	CircleRange=30;
	CameraAccel=0.0025f;
	VSpeed=0;	HSpeed=0;


}
CCameraMgr::~CCameraMgr(){

};

void CCameraMgr::Process(VECTOR Position,float Height)
{

	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_17)&&AngleV<=1.2f) VSpeed+=CameraAccel;
	else if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_19)&&AngleV>=-0.6f) VSpeed-=CameraAccel;
	else{
		if(VSpeed>CameraAccel*4) VSpeed-=CameraAccel*4;	else if(VSpeed<-CameraAccel*4) VSpeed+=CameraAccel*4;	else VSpeed=0;
	}
	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_20)) HSpeed+=CameraAccel;
	else if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_18)) HSpeed-=CameraAccel;
	else{
		if(HSpeed>CameraAccel*4) HSpeed-=CameraAccel*4;	else if(HSpeed<-CameraAccel*4) HSpeed+=CameraAccel*4;	else HSpeed=0;
	}
	if(VSpeed>=CameraSpeed) VSpeed=CameraSpeed;	else if(VSpeed<=-CameraSpeed) VSpeed=-CameraSpeed;
	if(HSpeed>=CameraSpeed) HSpeed=CameraSpeed;	else if(HSpeed<=-CameraSpeed) HSpeed=-CameraSpeed;
	
	AngleV+=VSpeed;	AngleH+=HSpeed;

	if((CheckHitKey(KEY_INPUT_Z))&&AngleV<=1.2f) AngleV+=CameraSpeed;
	if((CheckHitKey(KEY_INPUT_W))&&AngleV>=-0.6f) AngleV-=CameraSpeed;
	if(CheckHitKey(KEY_INPUT_A)) AngleH+=CameraSpeed;
	if(CheckHitKey(KEY_INPUT_S)) AngleH-=CameraSpeed;	
	
	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_14)) CameraSwitch_=RIGHT;
	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_15)) CameraSwitch_=LEFT;
	
	CenterVector = VGet(sinf(AngleH)*CameraDistance*cosf(AngleV),sinf(AngleV)*CameraDistance,-cosf(AngleH)*CameraDistance*cosf(AngleV));

	CameraVector = VAdd(Position,VGet(cosf(AngleH)*0.4*CameraSwitch_,Height,sinf(AngleH)*0.4*CameraSwitch_));

	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_5)||CheckHitKey(KEY_INPUT_TAB) ){

		CameraSpeed=0.016f;

		SetCameraPositionAndTarget_UpVecY(VAdd(CameraVector,VScale(CenterVector,0.15)),VSub(CameraVector,CenterVector));

	}
	else{
		CameraSpeed=0.04f;

		//(,,)の視点から(x,y,z)のターゲットを見る角度にカメラを設置
		SetCameraPositionAndTarget_UpVecY(VAdd(CameraVector,CenterVector),CameraVector) ;
    
	}
	
}	

void CCameraMgr::Draw()
{
	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_5)||CheckHitKey(KEY_INPUT_TAB)){
	
		DrawCircle(CenterH,CenterV,CircleRange,GetColor(255,255,0),FALSE);
	}

}
#include"DxLib.h"
#include"Stage.h"


CStage::CStage(){

	SelectStageFlag=0;
	StageNumber=STAGE1;

    // ３Ｄモデルの０番目のアニメーションをアタッチする
    GroundAnime= MV1AttachAnim(GroundHandle, 0, -1, FALSE ) ;
    // 再生時間の初期化
    PlayTime = 0.0f ;

}
CStage::~CStage(){}


void CStage::Process()
{
	
   // アタッチしたアニメーションの総再生時間を取得する
	TotalTime = MV1GetAttachAnimTotalTime( GroundHandle, 0) ;


	PlayTime+=0.4f;
	if( PlayTime >= TotalTime ){
		PlayTime = 0.0f ;
	}
	
	MV1SetAttachAnimTime( GroundHandle,0, PlayTime ) ;
	MV1SetAttachAnimTime( GroundHandle,1, PlayTime ) ;
	MV1SetAttachAnimTime( SkyHandle,0, PlayTime ) ;

}

void CStage::Draw()
{

	MV1DrawModel(SkyHandle);
	MV1DrawModel(GroundHandle);

}

void CStage::Init()
{
	switch(StageNumber){

	case(STAGE1):	BatokinInit();		break;
	case(STAGE2):	RoomInit();	break;
	case(STAGE3):	VRInit();			break;
	case(STAGE4):	GarekiInit();		break;
	case(STAGE5):	KelorinInit();		break;

	}


}

void CStage::BatokinInit()
{
	SkyHandle=MV1LoadModel("dat/バトーキン島改/skydome.x");
	GroundHandle=MV1LoadModel("dat/バトーキン島改/batokin_island改.mv1");

	MV1SetScale(GroundHandle,VGet(0.2f,0.2f,0.2f));
	MV1SetupCollInfo(GroundHandle,-1,8,8,8);

	AttachIndex[SEA]=MV1AttachAnim( GroundHandle, SEA,-1,FALSE ) ;
	AttachIndex[WATERFALL]=MV1AttachAnim( GroundHandle, WATERFALL,-1,FALSE ) ;
	AttachIndex[3]=MV1AttachAnim( SkyHandle, 0,-1,FALSE ) ;

}
void CStage::RoomInit()
{
	//SkyHandle=MV1LoadModel("dat/clock/時計ステージ.pmd");
	//GroundHandle=MV1LoadModel("dat/clock/時計ステージ用ガラス（pmd用）.x");

	GroundHandle=MV1LoadModel("dat/nazo/Model/普通の部屋.pmd");
	//MV1SetPosition(GroundHandle,VGet(0,-25,0));
	//MV1SetPosition(SkyHandle,VGet(0,-25,0));

	//MV1SetScale(GroundHandle,VGet(10.f,10.f,10.f));
	MV1SetupCollInfo(GroundHandle,-1,8,8,8);

}
void CStage::VRInit()
{
	SkyHandle=MV1LoadModel("dat/VR_SYSTEM/sora.x");
	GroundHandle=MV1LoadModel("dat/VR_SYSTEM/VR_SYSTEM.x");

	MV1SetScale(GroundHandle,VGet(4.f,4.f,4.f));

	MV1SetupCollInfo(GroundHandle,-1,16,16,16);

}
void CStage::GarekiInit()
{
	SkyHandle=MV1LoadModel("dat/ガレキ町1.0セット/ガレキ町1.0/ガレキ遠景ドーム.x");
	GroundHandle=MV1LoadModel("dat/ガレキ町1.0セット/ガレキ町1.0/ガレキ町１．０.x");

	//MV1SetScale(GroundHandle,VGet(4.f,4.f,4.f));

	MV1SetupCollInfo(GroundHandle,-1,8,8,8);
}
void CStage::KelorinInit()
{
	SkyHandle=MV1LoadModel("dat/バトーキン島改/skydome.x");

	//SkyHandle=MV1LoadModel("dat/ケロリン町2.02a/ケロリン遠景a00.x");
	GroundHandle=MV1LoadModel("dat/ケロリン町2.02a/ケロ町メイン２．０2.mv1");

	MV1SetScale(GroundHandle,VGet(0.1f,0.1f,0.1f));
	MV1SetPosition(GroundHandle,VGet(0,2.2,0));
	MV1SetupCollInfo(GroundHandle,-1,8,8,8);
}

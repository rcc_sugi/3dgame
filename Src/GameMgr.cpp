#include"DxLib.h"
#include "GameMgr.h"

CGameMgr::CGameMgr(){

	DrawFormatString(0,20,GetColor(0,255,0),"gamemgr_complete");
	ScreenFlip();

	pCharaMgr		=new CCharaMgr();
	pStage			=new CStage();
	pCollisionMgr	=new CCollisionMgr();
	pOption			=new COption();

}
CGameMgr::~CGameMgr(){

	delete pCharaMgr;
	delete pStage;
	delete pCollisionMgr;
	delete pOption;
}

bool CGameMgr::Process()
{

	pOption->Process();
	pCharaMgr->Process();
	pCollisionMgr->Process(pCharaMgr,pStage);
	pStage->Process();

	return TRUE;
}

void CGameMgr::Draw()
{
	
	pStage->Draw();
	pCharaMgr->Draw();

	pOption->Draw();
	if(pCollisionMgr->DrawDataFlag==1){

		DrawFormatString(300,90,GetColor(0,255,0),"x:%f",pCharaMgr->pPlayer->pMotion->Position.x);
		DrawFormatString(300,105,GetColor(0,255,0),"y:%f",pCharaMgr->pPlayer->pMotion->Position.y);
		DrawFormatString(300,120,GetColor(0,255,0),"z:%f",pCharaMgr->pPlayer->pMotion->Position.z);
		DrawFormatString(300,150,GetColor(0,255,0),"speed:%f",pCharaMgr->pPlayer->pMotion->MoveSpeed);

	}

}

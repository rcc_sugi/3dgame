#include"DxLib.h"
#include"CollisionMgr.h"

CCollisionMgr::CCollisionMgr(){
	DrawFormatString(0,180,GetColor(0,255,0),"collision_complete");
	ScreenFlip();

	Count=0;
	HitFlag=0;
	DrawDataFlag=0;	DataTime=0;
	Gravity=0.02f;
	MaxY=0;	oldMaxY=0;
}
CCollisionMgr::~CCollisionMgr(){


}

void CCollisionMgr::Process(CCharaMgr* pCharaMgr,CStage* pStage)
{

	// プレイヤーの周囲にあるステージポリゴンを取得する
	// ( 検出する範囲は移動距離も考慮する )
	
	HitDim = MV1CollCheck_Sphere( pStage->GroundHandle, -1, pCharaMgr->pPlayer->pMotion->Position
		, PLAYER_ENUM_DEFAULT_SIZE + pCharaMgr->pPlayer->pMotion->MoveSpeed+pCharaMgr->pPlayer->pMotion->y ) ;
	
	// 検出されたポリゴンが壁ポリゴン( ＸＺ平面に垂直なポリゴン )か床ポリゴン( ＸＺ平面に垂直ではないポリゴン )かを判断する
	
		// 壁ポリゴンと床ポリゴンの数を初期化する
		KabeNum = 0 ;
		YukaNum = 0 ;

		// 検出されたポリゴンの数だけ繰り返し
		for( i = 0 ; i < HitDim.HitNum ; i ++ )
		{
			// ＸＺ平面に垂直かどうかはポリゴンの法線のＹ成分が０に限りなく近いかどうかで判断する
			if( HitDim.Dim[ i ].Normal.y <0.4 && HitDim.Dim[ i ].Normal.y > -0.4)
			{
				// 壁ポリゴンと判断された場合でも、プレイヤーのＹ座標＋0.1ｆより高いポリゴンのみ当たり判定を行う
				if( HitDim.Dim[ i ].Position[ 0 ].y > pCharaMgr->pPlayer->pMotion->Position.y + 0.1f ||
					HitDim.Dim[ i ].Position[ 1 ].y > pCharaMgr->pPlayer->pMotion->Position.y + 0.1f ||
					HitDim.Dim[ i ].Position[ 2 ].y > pCharaMgr->pPlayer->pMotion->Position.y + 0.1f )
				{
					// ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
					if( KabeNum < PLAYER_MAX_HITCOLL )
					{
						// ポリゴンの構造体のアドレスを壁ポリゴンポインタ配列に保存する
						Kabe[ KabeNum ] = &HitDim.Dim[ i ] ;

						// 壁ポリゴンの数を加算する
						KabeNum ++ ;
					}
				}
			}
			else
			{
				// ポリゴンの数が列挙できる限界数に達していなかったらポリゴンを配列に追加
				if( YukaNum < PLAYER_MAX_HITCOLL )
				{
					// ポリゴンの構造体のアドレスを床ポリゴンポインタ配列に保存する
					Yuka[ YukaNum ] = &HitDim.Dim[ i ] ;

					// 床ポリゴンの数を加算する
					YukaNum ++ ;
				}
			}
		}
	

	// 壁ポリゴンとの当たり判定処理
	if( KabeNum != 0 )
	{
		// 壁に当たったかどうかのフラグは初期状態では「当たっていない」にしておく
		HitFlag = 0 ;

		// 移動したかどうかで処理を分岐
		if( pCharaMgr->pPlayer->pMotion->MoveSpeed>0 )
		{
			// 壁ポリゴンの数だけ繰り返し
			for( i = 0 ; i < KabeNum ; i ++ )
			{
				// i番目の壁ポリゴンのアドレスを壁ポリゴンポインタ配列から取得
				Poly = Kabe[ i ] ;

				// ポリゴンとプレイヤーが当たっていなかったら次のカウントへ
				if( HitCheck_Capsule_Triangle( pCharaMgr->pPlayer->pMotion->Position, VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ) )
					, PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == FALSE ) continue ;

				// ここにきたらポリゴンとプレイヤーが当たっているということなので、ポリゴンに当たったフラグを立てる
				HitFlag = 1 ;

				// 壁に当たったら壁に遮られない移動成分分だけ移動する
				{
					VECTOR SlideVec ;	// プレイヤーをスライドさせるベクトル

					// 進行方向ベクトルと壁ポリゴンの法線ベクトルに垂直なベクトルを算出
					SlideVec = VCross( pCharaMgr->pPlayer->pMotion->MoveVector, Poly->Normal ) ;

					// 算出したベクトルと壁ポリゴンの法線ベクトルに垂直なベクトルを算出、これが
					// 元の移動成分から壁方向の移動成分を抜いたベクトル
					SlideVec = VCross( Poly->Normal, SlideVec ) ;

					// それを移動前の座標に足したものを新たな座標とする
					pCharaMgr->pPlayer->pMotion->Position = VAdd( pCharaMgr->pPlayer->pMotion->oldPosition, SlideVec ) ;
				}

				// 新たな移動座標で壁ポリゴンと当たっていないかどうかを判定する
				for( j = 0 ; j < KabeNum ; j ++ )
				{
					// j番目の壁ポリゴンのアドレスを壁ポリゴンポインタ配列から取得
					Poly = Kabe[ j ] ;

					// 当たっていたらループから抜ける
					if( HitCheck_Capsule_Triangle( pCharaMgr->pPlayer->pMotion->Position, VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ) )
						, PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == TRUE ) break ;
				}

				// j が KabeNum だった場合はどのポリゴンとも当たらなかったということなので
				// 壁に当たったフラグを倒した上でループから抜ける
				if( j == KabeNum )
				{
					HitFlag = 0 ;
					break ;
				}
			}
		}
		else
		{
			// 移動していない場合の処理
			
			// 壁ポリゴンの数だけ繰り返し
			for( i = 0 ; i < KabeNum ; i ++ )
			{
				// i番目の壁ポリゴンのアドレスを壁ポリゴンポインタ配列から取得
				Poly = Kabe[ i ] ;

				// ポリゴンに当たっていたら当たったフラグを立てた上でループから抜ける
				if( HitCheck_Capsule_Triangle( pCharaMgr->pPlayer->pMotion->Position, VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ) ), PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == TRUE )
				{
					HitFlag = 1 ;
					break ;
				}
			}
		}

		// 壁に当たっていたら壁から押し出す処理を行う
		if( HitFlag == 1 )
		{
			// 壁からの押し出し処理を試みる最大数だけ繰り返し
			for( k = 0 ; k < PLAYER_HIT_TRYNUM ; k ++ )
			{
				// 壁ポリゴンの数だけ繰り返し
				for( i = 0 ; i < KabeNum ; i ++ )
				{
					// i番目の壁ポリゴンのアドレスを壁ポリゴンポインタ配列から取得
					Poly = Kabe[ i ] ;

					// プレイヤーと当たっているかを判定
					if( HitCheck_Capsule_Triangle( pCharaMgr->pPlayer->pMotion->Position, VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ) ), PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == FALSE ) continue ;

					// 当たっていたら規定距離分プレイヤーを壁の法線方向に移動させる
					pCharaMgr->pPlayer->pMotion->Position = VAdd( pCharaMgr->pPlayer->pMotion->Position, VScale( Poly->Normal, PLAYER_HIT_SLIDE_LENGTH ) ) ;

					// 移動した上で壁ポリゴンと接触しているかどうかを判定
					for( j = 0 ; j < KabeNum ; j ++ )
					{
						// 当たっていたらループを抜ける
						Poly = Kabe[ j ] ;
						if( HitCheck_Capsule_Triangle( pCharaMgr->pPlayer->pMotion->Position, VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ) ), PLAYER_HIT_WIDTH, Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) == TRUE ) break ;
					}

					// 全てのポリゴンと当たっていなかったらここでループ終了
					if( j == KabeNum ) break ;
				}

				// i が KabeNum ではない場合は全部のポリゴンで押し出しを試みる前に全ての壁ポリゴンと接触しなくなったということなのでループから抜ける
				if( i != KabeNum ) break ;
			}
		}
	}

	// 床ポリゴンとの当たり判定
	if( YukaNum != 0 )
	{
			// 下降中かジャンプ中ではない場合の処理

			// 床ポリゴンに当たったかどうかのフラグを倒しておく
			HitFlag = 0 ;

			// 一番高い床ポリゴンにぶつける為の判定用変数を初期化
			MaxY = 0.0f ;

			// 床ポリゴンの数だけ繰り返し
			for( i = 0 ; i < YukaNum ; i ++ )
			{
				// i番目の床ポリゴンのアドレスを床ポリゴンポインタ配列から取得
				Poly = Yuka[ i ] ;

				LineRes = HitCheck_Line_Triangle( VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ) ), VAdd( pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, -4.0f, 0.0f ) ), Poly->Position[ 0 ], Poly->Position[ 1 ], Poly->Position[ 2 ] ) ;
	
				// 当たっていなかったら何もしない
				if( LineRes.HitFlag == FALSE ) continue ;

				// 既に当たったポリゴンがあり、且つ今まで検出した床ポリゴンより低い場合は何もしない
				if( HitFlag == 1 && MaxY > LineRes.Position.y ) continue ;

				// ポリゴンに当たったフラグを立てる
				HitFlag = 1 ;

				// 接触したＹ座標を保存する
				MaxY = LineRes.Position.y ;
			}

			// 床ポリゴンに当たったかどうかで処理を分岐
			if( HitFlag == 1 )
			{
				// 当たった場合

				// 接触したポリゴンで一番高いＹ座標をプレイヤーのＹ座標にする
	
				pCharaMgr->pPlayer->pMotion->Position.y= MaxY ;
				FallSpeed=0;
				pCharaMgr->pPlayer->pPlayerData->OnGroundFlag=1;
			
			
			}
			else{
				FallSpeed+=Gravity;
				pCharaMgr->pPlayer->pMotion->Position.y-=FallSpeed;
				pCharaMgr->pPlayer->pPlayerData->OnGroundFlag=0;

			}
	}
	else{
		FallSpeed+=Gravity;
		pCharaMgr->pPlayer->pMotion->Position.y-=FallSpeed;
		pCharaMgr->pPlayer->pPlayerData->OnGroundFlag=0;

	}
	
    // Ｚバッファを有効にする
    SetUseZBuffer3D( TRUE ) ;
    // Ｚバッファへの書き込みを有効にする
    SetWriteZBuffer3D( TRUE ) ;
	
	if(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_9 || CheckHitKey(KEY_INPUT_D)) DataTime++;
	else DataTime=0;

	if(DataTime==1){
		if(DrawDataFlag==0) DrawDataFlag=1;
		else if(DrawDataFlag==1) DrawDataFlag=0;
	}

	if(DrawDataFlag==1){
		// ３Ｄ空間上に球を描画する
		DrawSphere3D(pCharaMgr->pPlayer->pMotion->Position,PLAYER_ENUM_DEFAULT_SIZE + pCharaMgr->pPlayer->pMotion->MoveSpeed, 32, GetColor( 0,255,0 ), GetColor( 255, 255, 255 ), FALSE ) ;
		// ３Ｄ空間上にカプセルを描画する
		DrawCapsule3D( pCharaMgr->pPlayer->pMotion->Position, VAdd(pCharaMgr->pPlayer->pMotion->Position, VGet( 0.0f, pCharaMgr->pPlayer->pPlayerData->Height, 0.0f ))
			,PLAYER_HIT_WIDTH, 32,GetColor(0,255,0),GetColor(255,0,0),FALSE) ;

	}
	if(pCharaMgr->pPlayer->pMotion->Position.y<-20) pCharaMgr->pPlayer->pMotion->Position=VGet(0,0,0);
	/*
	DrawFormatString(300,60,GetColor(0,255,0),"hitnu:%d",HitDim.HitNum);
	DrawFormatString(300,75,GetColor(0,255,0),"frameindex:%d",HitDim.Dim->FrameIndex);
	DrawFormatString(300,90,GetColor(0,255,0),"hitflag:%d",HitDim.Dim->HitFlag);
	

	DrawFormatString(300,165,GetColor(0,255,0),"KabeNum:%d",KabeNum);
	DrawFormatString(300,180,GetColor(0,255,0),"YukaNum:%d",YukaNum);
	DrawFormatString(300,195,GetColor(0,255,0),"hitdimposition:%f",HitDim.Dim[ i ].Position[ 0 ].y);
	DrawFormatString(300,210,GetColor(0,255,0),"normaldim:%f",HitDim.Dim[ i ].Normal.y);
	*/

	// 検出したプレイヤーの周囲のポリゴン情報を開放する
	MV1CollResultPolyDimTerminate( HitDim ) ;

	oldMaxY=MaxY;
}
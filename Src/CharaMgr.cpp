#include"DxLib.h"
#include"CharaMgr.h"

CCharaMgr::CCharaMgr(){

	DrawFormatString(0,40,GetColor(0,255,0),"charamgr_complete");
	ScreenFlip();

	pPlayer			=new CPlayer();
	pEnemyMgr		=new CEnemyMgr();
	
}
CCharaMgr::~CCharaMgr(){

	delete pPlayer;
	delete pEnemyMgr;

}

void CCharaMgr::Process()
{
	
	pPlayer->Process();
	//pEnemyMgr->Process(pPlayer);
}
void CCharaMgr::Draw()
{
	pPlayer->Draw();
	
	//pEnemyMgr->Draw();
	
}
#ifndef PLAYER
#define PLAYER

#include"CameraMgr.h"
#include"JoyPad.h"
#include"Motion.h"
#include"PlayerData.h"

class CPlayer{

public:

	CPlayer();
	~CPlayer();

	CPlayerData* pPlayerData;
	CCameraMgr* pCameraMgr;
	CMotion* pMotion;

	enum ePCharacter{MIKU,REIMU/*,RIN,REN,TACHIKOMA,METALGEARREX*/,LAST};
	int PCharacter;

	void Process();
	void Draw();
	void Init();

	bool SelectCharaFlag;

};

#endif
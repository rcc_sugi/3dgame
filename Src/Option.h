#ifndef OPTION
#define OPTION


class COption{

public:
	COption();
	~COption();

	void Process();
	void Draw();

	enum eOption{Return,Init,End,LAST};
	eOption OptionMode;

	int StartTime,UpTime,DownTime;
	bool OptionFlag,InitFlag,EndFlag;

	int CenterH,CenterV,SelectPosition;
};

#endif OPTION
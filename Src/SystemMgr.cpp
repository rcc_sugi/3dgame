#include"DxLib.h"
#include "SystemMgr.h"
#include"JoyPad.h"

bool TitleFlag;

CSystemMgr::CSystemMgr(){

	TitleFlag=0;
	ScreenSize=720;

	ChangeWindowMode(TRUE)
	,SetMainWindowText("3DGame")
	,SetGraphMode(ScreenSize/9*16,ScreenSize,16)
	, DxLib_Init()
	, SetDrawScreen( DX_SCREEN_BACK );

	DrawFormatString(0,0,GetColor(0,255,0),"systemmgr_complete");
	DrawFormatString(ScreenSize*16/9-140,ScreenSize-40,GetColor(0,255,0),"NOW LOADING…");
	ScreenFlip();

	pFps=new Fps();
	pGameMgr=new CGameMgr();

	GameState_=TITLE;	iTitleState_=START;

	PressCircleTime=0;	PressXTime=0;	PressUpTime=0;	PressDownTime=0;
	TitleFlag=0;	SetUpFlag=0;	QuitFlag=0;
	PreviewHandle=-1;

	pGameMgr->pCharaMgr->pPlayer->pCameraMgr->CenterH=ScreenSize/9*8;
	pGameMgr->pCharaMgr->pPlayer->pCameraMgr->CenterV=ScreenSize/2;
	
	pGameMgr->pOption->CenterH=ScreenSize/9*8;
	pGameMgr->pOption->CenterV=ScreenSize/2;

	
}
CSystemMgr::~CSystemMgr(){

	delete pFps;
	delete pGameMgr;

}

void CSystemMgr::MainLoop()
{
	// ------------------------
	// メインループ
	// ------------------------
	while(!ScreenFlip()&&!ProcessMessage()&&!ClearDrawScreen()
		&&!CheckHitKey(KEY_INPUT_ESCAPE)&&!QuitFlag){
	

			switch(GameState_){
			case(DEMO):		;break;
			case(TITLE):	Title(); break;
			case(GAME):
				//計算処理
				pGameMgr->Process();
				//描画処理
				pGameMgr->Draw();
				if(CheckHitKey(KEY_INPUT_BACK)){

					TitleFlag=0;
					SetUpFlag=0;
					pGameMgr->pCharaMgr->pPlayer->SelectCharaFlag=0;
					pGameMgr->pStage->SelectStageFlag=0;
					
					GameState_=TITLE;
				}
				if(pGameMgr->pOption->InitFlag==1){
					
					pGameMgr->pCharaMgr->pPlayer->pMotion->Position = VGet(0,0,0);
					pGameMgr->pOption->InitFlag=0;

				}
				else if(pGameMgr->pOption->EndFlag==1){
					GameState_ = TITLE;

					delete pGameMgr;
					ScreenFlip();

					TitleFlag=0;

					pGameMgr = new CGameMgr();
					
					pGameMgr->pCharaMgr->pPlayer->pCameraMgr->CenterH=ScreenSize/9*8;
					pGameMgr->pCharaMgr->pPlayer->pCameraMgr->CenterV=ScreenSize/2;
	
					pGameMgr->pOption->CenterH=ScreenSize/9*8;
					pGameMgr->pOption->CenterV=ScreenSize/2;

				}
				break;
			}
				
			//Fps管理
			pFps->Update();
			pFps->Draw();
			pFps->Wait();


	}
}

void CSystemMgr::Title()
{
	
		if(CheckHitKey(KEY_INPUT_DOWN)||(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_DOWN)) PressDownTime++;
		else PressDownTime=0;
		if(CheckHitKey(KEY_INPUT_UP)||(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_UP)) PressUpTime++;
		else PressUpTime=0;
		if(CheckHitKey(KEY_INPUT_RETURN)||(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_2)) PressCircleTime++;
		else PressCircleTime=0;
		if(CheckHitKey(KEY_INPUT_BACK)||(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_3)) PressXTime++;
		else PressXTime=0;

			//状態を示す数字
			DrawFormatString(10,285,GetColor(0,255,0),"%d",GameState_);
			DrawFormatString(10,300,GetColor(0,255,0),"%d",iTitleState_);
			DrawFormatString(10,315,GetColor(0,255,0),"%d",pGameMgr->pCharaMgr->pPlayer->PCharacter);
			DrawFormatString(10,330,GetColor(0,255,0),"%d",pGameMgr->pStage->StageNumber);

	if(TitleFlag==0){

		PreviewHandle = LoadGraph("dat/図2.bmp");

		if(PressDownTime==1)	iTitleState_=(iTitleState_+1)%LAST;
		else if(PressUpTime==1) iTitleState_=(iTitleState_+LAST-1)%LAST;
		else if(PressCircleTime==1) TitleFlag=1;
		DrawExtendGraph(0,0,1280,720,PreviewHandle,TRUE);

			switch(iTitleState_){
			case(START):	DrawFormatString(900,600,GetColor(0,255,0),"はじめる");		break;
			case(CONTINUE):	DrawFormatString(900,600,GetColor(0,255,0),"つづける");		break;
			case(SETTING):	DrawFormatString(900,600,GetColor(0,255,0),"オプション");	break;
			case(QUIT):		DrawFormatString(900,600,GetColor(0,255,0),"やめる");		break;
			}

	}
	else if(TitleFlag==1){


		switch(iTitleState_){
		case(START):	SetUp();		break;
		case(CONTINUE):	SetUp();		break;
		case(SETTING):	TitleFlag=0;	break;
		case(QUIT):		QuitFlag=1;		break;
		}
	}
}

void CSystemMgr::SetUp()
{

	if(pGameMgr->pCharaMgr->pPlayer->SelectCharaFlag==0){

		if(PressDownTime==1)	pGameMgr->pCharaMgr->pPlayer->PCharacter = (pGameMgr->pCharaMgr->pPlayer->PCharacter+1)%pGameMgr->pCharaMgr->pPlayer->LAST;
		else if(PressUpTime==1) pGameMgr->pCharaMgr->pPlayer->PCharacter = (pGameMgr->pCharaMgr->pPlayer->PCharacter+pGameMgr->pCharaMgr->pPlayer->LAST-1)%pGameMgr->pCharaMgr->pPlayer->LAST;
		else if(PressCircleTime==1){

			DrawFormatString(1280-140,720-40,GetColor(0,255,0),"NOW LOADING…");
			ScreenFlip();	

			pGameMgr->pCharaMgr->pPlayer->Init();
			pGameMgr->pCharaMgr->pPlayer->SelectCharaFlag=1;
		}
		else if(PressXTime==1) TitleFlag=0;
		else{
			DrawFormatString(0,70,GetColor(0,255,0),"キャラクタ選択");

			switch(pGameMgr->pCharaMgr->pPlayer->PCharacter){
			case(pGameMgr->pCharaMgr->pPlayer->MIKU):			
				DrawFormatString(30,100,GetColor(0,255,0),"ミク");
				PreviewHandle=LoadGraph("dat/Lat式ミク/miku.bmp");

				break;
			case(pGameMgr->pCharaMgr->pPlayer->REIMU):
				DrawFormatString(30,100,GetColor(0,255,0),"霊夢");
				PreviewHandle=LoadGraph("dat/博麗霊夢X/reimu.bmp");
				
				break;
/*			case(pGameMgr->pCharaMgr->pPlayer->RIN):
				DrawFormatString(30,100,GetColor(0,255,0),"リン");
				PreviewHandle=-1;
				break;
			case(pGameMgr->pCharaMgr->pPlayer->REN):
				DrawFormatString(30,100,GetColor(0,255,0),"レン");
				PreviewHandle=-1;
				
				break;
			case(pGameMgr->pCharaMgr->pPlayer->TACHIKOMA):
				DrawFormatString(30,100,GetColor(0,255,0),"タチコマ");
				PreviewHandle=-1;
				break;
			case(pGameMgr->pCharaMgr->pPlayer->METALGEARREX):
				DrawFormatString(30,100,GetColor(0,255,0),"メタルギア");
				PreviewHandle=-1;
				break;
				*/

			}
			DrawExtendGraph(0,-30,1024,738,PreviewHandle,TRUE);
		}
	}
	else if(pGameMgr->pCharaMgr->pPlayer->SelectCharaFlag==1){

		if(PressDownTime==1)	pGameMgr->pStage->StageNumber=(pGameMgr->pStage->StageNumber+1)%pGameMgr->pStage->LAST;
		else if(PressUpTime==1) pGameMgr->pStage->StageNumber=(pGameMgr->pStage->StageNumber+pGameMgr->pStage->LAST-1)%pGameMgr->pStage->LAST;
		else if(PressCircleTime==1){

			pGameMgr->pStage->Init();
			pGameMgr->pStage->SelectStageFlag=1;

		}
		else if(PressXTime==1) pGameMgr->pCharaMgr->pPlayer->SelectCharaFlag=0;
		else{

			DrawFormatString(0,70,GetColor(0,255,0),"ステージ選択");
		
			switch(pGameMgr->pStage->StageNumber){
			case(pGameMgr->pStage->STAGE1):		PreviewHandle=LoadGraph("dat/バトーキン島/batokin.bmp");
				DrawFormatString(30,100,GetColor(0,255,0),"バトーキン島");	break;

			case(pGameMgr->pStage->STAGE2):		PreviewHandle = LoadGraph("dat/nazo/Model/room.bmp");
				DrawFormatString(30,100,GetColor(0,255,0),"普通の部屋");	break;

			case(pGameMgr->pStage->STAGE3):	PreviewHandle = LoadGraph("dat/VR_SYSTEM/VR_SYSTEM.bmp");	
				DrawFormatString(30,100,GetColor(0,255,0),"VR_SYSTEM");		break;

			case(pGameMgr->pStage->STAGE4):	PreviewHandle = LoadGraph("dat/ガレキ町1.0セット/ガレキ町1.0/gareki.bmp");;
				DrawFormatString(30,100,GetColor(0,255,0),"ガレキ町（工事中）");		break;

			case(pGameMgr->pStage->STAGE5):	PreviewHandle = LoadGraph("dat/ケロリン町2.02a/kelorin.bmp");;
				DrawFormatString(30,100,GetColor(0,255,0),"ケロリン町（工事中）");	break;

			}
		}
		DrawExtendGraph(300,0,1324,768,PreviewHandle,FALSE);

	}
	if(pGameMgr->pCharaMgr->pPlayer->SelectCharaFlag&&pGameMgr->pStage->SelectStageFlag) GameState_=GAME;
}
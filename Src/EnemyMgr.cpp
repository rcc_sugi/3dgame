#include"DxLib.h"
#include"EnemyMgr.h"
#include<math.h>


CEnemyMgr::CEnemyMgr()
{
	DrawFormatString(0,160,GetColor(0,255,0),"enemymgr_complete");
	ScreenFlip();

	EnemyHandle=MV1LoadModel("dat/ガレキ町1.0セット/ヒャッハー1.0/ヒャッハー１．０金.mv1");
	MV1SetScale(EnemyHandle,VGet(0.09f,0.09f,0.09f));
	AttachIndex[0]=MV1AttachAnim( EnemyHandle, 0,-1,FALSE ) ;
	PlayTime=0.f;

	Rotate=0;
	AnglePE=0;

	Position=VGet(-7,0,6);
	HP=1000;
	Speed=0.4;
	EyeRange=1;
	EyeDistance=6;
	EarRange=0.4;
	AngleE=0;

}
CEnemyMgr::~CEnemyMgr(){}

void CEnemyMgr::Init()
{
	


}

void CEnemyMgr::Process(CPlayer* pPlayer)
{

	MV1SetPosition( EnemyHandle,Position);
	
	VectorPE=VSub( Position,pPlayer->pMotion->Position);
	DistancePE=VSize(VectorPE);
	
	if(VectorPE.x!=0) AnglePE=-atan(VectorPE.z/VectorPE.x);

	if(DistancePE < EyeDistance){
		
		if((-EyeRange) <AnglePE - AngleE && AnglePE - AngleE < (EyeRange)){
			AngleE=AnglePE;

			MV1SetRotationXYZ(EnemyHandle,VGet(0,AngleE - DX_PI_F/2,0));
		}
	}
	
		
	// アタッチしたアニメーションの総再生時間を取得する
	TotalTime = MV1GetAttachAnimTotalTime( EnemyHandle,AttachIndex[0] ) ;
	// 再生時間を進める
	PlayTime += 0.5f ;
	// 再生時間がアニメーションの総再生時間に達したら再生時間を０に戻す
	if( PlayTime >= TotalTime ){
		PlayTime = 0.0f ;
	}
	// 再生時間をセットする
	MV1SetAttachAnimTime(EnemyHandle, AttachIndex[0],PlayTime ) ;

}

void CEnemyMgr::Draw()
{
	MV1DrawModel(EnemyHandle ) ;

	/*
	DrawFormatString(400,415,GetColor(0,255,0),"angleE:%f",AngleE);
	DrawFormatString(400,430,GetColor(0,255,0),"anglepe:%f",AnglePE);
	DrawFormatString(400,445,GetColor(0,255,0),"distance:%f",DistancePE);
	DrawFormatString(400,460,GetColor(0,255,0),"pe.x:%f",VectorPE.x);
	DrawFormatString(400,475,GetColor(0,255,0),"pe.z:%f",VectorPE.z);
	DrawFormatString(400,490,GetColor(0,255,0),"playtime: %f",PlayTime);
	*/
	if(DistancePE<2) DrawFormatString(600,600,GetColor(0,255,0),"R1");

}
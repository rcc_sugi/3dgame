#ifndef STAGE
#define STAGE

class CStage{

public:
	CStage();
	~CStage();

	enum eStage{STAGE1,STAGE2,STAGE3,STAGE4,STAGE5,LAST};
	unsigned int StageNumber;
	bool SelectStageFlag;

	void Process();
	void Draw();
	void Init();

	void BatokinInit();
	void RoomInit();
	void VRInit();
	void GarekiInit();
	void KelorinInit();

	enum eStageAnime{SEA,WATERFALL};
	int AttachIndex[4];
	int SkyHandle;
	int GroundHandle,GroundAnime;

	float PlayTime;
	int TotalTime;
};


#endif
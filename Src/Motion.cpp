#include"DxLib.h"
#include"Motion.h"
#include<math.h>

CMotion::CMotion(){
	DrawFormatString(0,100,GetColor(0,255,0),"motion_complete");
	ScreenFlip();


	pJoyPad=new CJoyPad();

	XPressTime=0;	oldXPressTime=0;	R1PressTime=0;
	PostureSpeed=0.04f;
	Posture=STAND;	oldPosture=STAND;	tempPosture=STAND;
	Action=NONE;	oldAction=NONE;
	PChangeRate=1;
	playAnim=0.f;	TotalTime=0.f;	PlayTime=0.f;
	Anime=STAND_NONE;	oldAnime=-1;	tempAnime=-1;

	//pPlayerData->Height=pPlayerData->StandHeight;
	AngleP=DX_PI_F/2;	GoAngle=0;	oldGoAngle=0;
	Position=VGet(0,0,0); oldPosition=VGet(0,0,0);
	x=0;	y=0;	z=0;
	MoveSpeed=0;
	AChangeRate=0.04f;
	SquarePressTime=0;
	HoldFlag=0;

}
CMotion::~CMotion()
{
	delete pJoyPad;

}

void CMotion::Process(CCameraMgr* pCameraMgr,CPlayerData* pPlayerData)
{
	//Position.y=0;
	CMotion::pPlayerData=pPlayerData;

	pJoyPad->Process();

	oldXPressTime=XPressTime;
	oldSquarePressTime=SquarePressTime;
	
	if(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_3) XPressTime++;
	else XPressTime=0;
	if(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_6) R1PressTime++;
	else R1PressTime=0;
	if(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_4) SquarePressTime++;
	else SquarePressTime=0;
	if(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_5 && Anime!=DIVE) HoldFlag=1;
	else HoldFlag=0;

	MoveRange=(int)VSize(VGet((float)pJoyPad->InputX,(float)pJoyPad->InputY,0));
	
	//////////////////////////////
	// アクション処理
	if(MoveRange==0)		Action=NONE;
	else if(MoveRange<=800)	Action=WALK;
	else if(MoveRange>800)	Action=RUN;

	// hold中
	if(Action==RUN && HoldFlag==1) Action = WALK;
	
	//プレイヤーの向き
	oldGoAngle=GoAngle;
	GoAngle=AngleP-pCameraMgr->AngleH;

	//メタルギア風モーション
	MotionMG();

	if(pJoyPad->VInputX>0){					//X方向に正のとき
		AngleP=atan((float)(pJoyPad->VInputY/pJoyPad->VInputX));
		x=cos(GoAngle)*MoveSpeed;
		z=-sin(GoAngle)*MoveSpeed;
	}
	else if(pJoyPad->VInputX<0){			//X方向に負のとき
		AngleP=(float)atan(pJoyPad->VInputY/pJoyPad->VInputX)+DX_PI_F;
		x=cos(GoAngle)*MoveSpeed;
		z=-sin(GoAngle)*MoveSpeed;
	}
	else{									//X=0のとき
		if(pJoyPad->VInputY>0){
			AngleP=DX_PI_F/2;
			x=cos(GoAngle)*MoveSpeed;
			z=-sin(GoAngle)*MoveSpeed;
		}
		else if(pJoyPad->VInputY<0){
			AngleP=-DX_PI_F/2;
			x=cos(GoAngle)*MoveSpeed;
			z=-sin(GoAngle)*MoveSpeed;

		}
	}
	if(HoldFlag==1){
		
		GoAngle=pCameraMgr->AngleH*-1-DX_PI/2;
		MV1SetRotationXYZ( pPlayerData->PHandle, VGet( 0.0f,-DX_PI_F/2+GoAngle, 0.0f ));
	}

	else if(pJoyPad->VInputX!=0||pJoyPad->VInputY!=0) MV1SetRotationXYZ( pPlayerData->PHandle, VGet( 0.0f,-DX_PI_F/2+GoAngle, 0.0f ) ) ;
	
	y=Position.y-oldPosition.y;
	MoveVector=VGet(x,y,z);

	if(MoveVector.x!=0||MoveVector.z!=0){
		MoveVector=VScale(MoveVector,MoveSpeed/VSize(MoveVector));
	}

	if(Posture==CREEP&&HoldFlag) pCameraMgr->CameraDistance=2;
	else pCameraMgr->CameraDistance=4;

	oldPosition=Position;
	Position= VAdd(Position,MoveVector);
	MV1SetPosition( pPlayerData->PHandle,Position);


	//////////////////////////////////////////
	//アニメーションのつなぎ処理
	if(Anime!=tempAnime){

		MV1DetachAnim(pPlayerData->PHandle,AttachIndex[oldAnime]);

		AttachIndex[Anime]=MV1AttachAnim( pPlayerData->PHandle, Anime,-1,FALSE ) ;
		oldAnime=tempAnime;	//1回遅れの姿勢を記憶するoldAnime
		PChangeRate=1-PChangeRate;


	}

	tempAnime=Anime;

	if(PChangeRate>=0&&PChangeRate<1) PChangeRate+=AChangeRate;
	else if(PChangeRate<0) PChangeRate=0;
	else if(PChangeRate>=1){

		PChangeRate=1.f;

	}
	MV1SetAttachAnimBlendRate( pPlayerData->PHandle, AttachIndex[Anime], PChangeRate) ;
	MV1SetAttachAnimBlendRate( pPlayerData->PHandle, AttachIndex[oldAnime], 1.0f -PChangeRate) ;
	
	// アタッチしたアニメーションの総再生時間を取得する
	TotalTime = MV1GetAttachAnimTotalTime( pPlayerData->PHandle,AttachIndex[Anime] ) ;
	// 再生時間を進める
	PlayTime += 0.5f ;
	// 再生時間がアニメーションの総再生時間に達したら再生時間を０に戻す
	if( PlayTime >= TotalTime ){
		PlayTime = 0.0f ;
	}
	// 再生時間をセットする
	MV1SetAttachAnimTime( pPlayerData->PHandle, AttachIndex[Anime],PlayTime ) ;
	
}

void CMotion::MotionMG()
{
	/////////////////////
	//姿勢処理
	switch(Posture){
	case(STAND):
		
		if(oldXPressTime>0&&oldXPressTime<10&&XPressTime==0) Posture=CROUCH;

		if(pPlayerData->Height < pPlayerData->StandHeight-PostureSpeed) pPlayerData->Height+=PostureSpeed;
		else pPlayerData->Height=pPlayerData->StandHeight;

		break;
	case(CROUCH):

		if(oldXPressTime>0&&oldXPressTime<10&&XPressTime==0) Posture=STAND;
		else if(oldXPressTime==10) Posture=CREEP;

		if(pPlayerData->Height < pPlayerData->CrouchHeight-PostureSpeed) pPlayerData->Height+=PostureSpeed;
		else if(pPlayerData->Height > pPlayerData->CrouchHeight+PostureSpeed) pPlayerData->Height-=PostureSpeed;
		else pPlayerData->Height=pPlayerData->CrouchHeight;

		break;
	case(CREEP):
		if(oldXPressTime>0&&oldXPressTime<10&&XPressTime==0) Posture=CROUCH;
		
		if(pPlayerData->Height < pPlayerData->CreepHeight-PostureSpeed) pPlayerData->Height+=PostureSpeed;
		else if(pPlayerData->Height > pPlayerData->CreepHeight+PostureSpeed) pPlayerData->Height-=PostureSpeed;
		else pPlayerData->Height=pPlayerData->CreepHeight;


		break;
	}
	
	//////////////////////
	//アニメ処理
	switch(Anime){
	case(STAND_NONE):	StandNone()		;break;
	case(STAND_WALK):	StandWalk()		;break;
	case(STAND_RUN):	StandRun()		;break;
	case(CROUCH_NONE):	CrouchNone()	;break;
	case(CROUCH_WALK):	CrouchWalk()	;break;
	case(CROUCH_RUN):	CrouchRun()		;break;
	case(CREEP_NONE):	CreepNone()		;break;
	case(CREEP_WALK):	CreepWalk()		;break;
	case(CREEP_RUN):	CreepRun()		;break;
	case(DASH):			Dash()			;break;
	case(DIVE):			Dive()			;break;
	case(CQC1):			Cqc1()			;break;
	case(CQC2):			Cqc2()			;break;
	case(CQC3):			Cqc3()			;break;
	case(HOLD):;break;
	}

}
void CMotion::StandNone()
{
	Anime=STAND_NONE;
	MoveSpeed=0;

	if(pPlayerData->Stamina<pPlayerData->StaminaMax) pPlayerData->Stamina+=10;
	else pPlayerData->Stamina=pPlayerData->StaminaMax;

	if(PChangeRate==1){

		if(Posture==CROUCH){	Anime=CROUCH_NONE;	AChangeRate=0.08f;}
		else if(Action==WALK){	Anime=STAND_WALK;	PChangeRate=0;	AChangeRate=0.14f;}
		else if(Action==RUN){	Anime=STAND_WALK;	PChangeRate=0;	AChangeRate=0.08f;}
	}
}
void CMotion::StandWalk()
{
	Anime=STAND_WALK;
	MoveSpeed=pPlayerData->StandSpeed*0.4;

	if(PChangeRate==1){

		if(Posture==CROUCH){	Anime=CROUCH_WALK;	AChangeRate=0.08f;}
		else if(Action==NONE){	Anime=STAND_NONE;	AChangeRate=0.1f;}
		else if(Action==RUN){	Anime=STAND_RUN;	AChangeRate=0.08f;}

		else if(SquarePressTime>=10){	Anime=DASH;	AChangeRate=0.12f;}

	}
	

}
void CMotion::StandRun()
{
	Anime=STAND_RUN;
	MoveSpeed=pPlayerData->StandSpeed;

	if(PChangeRate>=0.8){

		if(Posture==CROUCH){	Anime=CROUCH_RUN;	AChangeRate=0.08f;}
		else if(Action==NONE){	Anime=STAND_WALK;	AChangeRate=0.14f;}
		else if(Action==WALK){	Anime=STAND_WALK;	AChangeRate=0.14f;}

		else if(SquarePressTime==0&&oldSquarePressTime>0){ Anime=DIVE; AChangeRate=0.16f;}
		else if(SquarePressTime>=10){	Anime=DASH;	AChangeRate=0.12f;}
	
	}
}
void CMotion::CrouchNone()
{
	Anime=CROUCH_NONE;
	MoveSpeed=0;

	if(PChangeRate==1){
		
		if(Posture==STAND){		Anime=STAND_NONE;	AChangeRate=0.08f;}
		else if(Posture==CREEP){Anime=CREEP_NONE;	AChangeRate=0.06f;}
		else if(Action==WALK){	Anime=CROUCH_WALK;	AChangeRate=0.16f;}
		else if(Action==RUN){	Anime=CROUCH_WALK;	AChangeRate=0.24f;}

	}
}
void CMotion::CrouchWalk()
{
	Anime=CROUCH_WALK;
	MoveSpeed=pPlayerData->CrouchSpeed*0.6;

	if(PChangeRate==1){

		if(Posture==STAND){		Anime=STAND_WALK;	AChangeRate=0.08f;}
		else if(Posture==CREEP){Anime=CREEP_WALK;	AChangeRate=0.06f;}
		else if(Action==RUN){	Anime=CROUCH_RUN;	AChangeRate=0.24f;}
		else if(Action==NONE){	Anime=CROUCH_NONE;	AChangeRate=0.2f;}
	}

}
void CMotion::CrouchRun()
{
	Anime=CROUCH_RUN;
	MoveSpeed=pPlayerData->CrouchSpeed;


	if(PChangeRate==1){

	if(Posture==STAND){		Anime=STAND_RUN;	AChangeRate	=0.08f;}
	else if(Posture==CREEP){Anime=CREEP_RUN;	AChangeRate	=0.06f;}
	else if(Action==WALK){	Anime=CROUCH_WALK;	AChangeRate	=0.14f;}
	else if(Action==NONE){	Anime=CROUCH_NONE;	AChangeRate	=0.14f;}
	else if(SquarePressTime==0&&oldSquarePressTime>0&&oldSquarePressTime<10){ Anime=DIVE; AChangeRate=0.12f;}
	

	}
}
void CMotion::CreepNone()
{
	MoveSpeed=0;

	if(PChangeRate==1){

	if(Posture==CROUCH){	Anime=CROUCH_NONE;	AChangeRate	=0.06f;}
	else if(Action==WALK){	Anime=CREEP_WALK;	AChangeRate	=0.08f;}
	else if(Action==RUN){	Anime=CREEP_RUN;	AChangeRate	=0.08f;}
	}
}
void CMotion::CreepWalk()
{

	MoveSpeed=pPlayerData->CreepSpeed*0.6;

	if(PChangeRate==1){

	if(Posture==CROUCH){	Anime=CROUCH_WALK;	AChangeRate	=0.06f;}
	else if(Action==NONE){	Anime=CREEP_NONE;	AChangeRate	=0.08f;}
	else if(Action==RUN){	Anime=CREEP_RUN;	AChangeRate	=0.08f;}
	}

}
void CMotion::CreepRun()
{
	MoveSpeed=pPlayerData->CreepSpeed;

	if(PChangeRate==1){

	if(Posture==CROUCH){	Anime=CROUCH_RUN;	AChangeRate	=0.06f;}
	else if(Action==WALK){	Anime=CREEP_WALK;	AChangeRate	=0.08f;}
	else if(Action==NONE){	Anime=CREEP_NONE;	AChangeRate	=0.08f;}

	}

}
void CMotion::Dash()
{
	MoveSpeed=pPlayerData->StandSpeed*2;
	pPlayerData->Stamina-=6;

	if(PChangeRate==1){
		if(SquarePressTime==0||Action==NONE){				Anime=STAND_RUN;	AChangeRate=0.12;}
		else if(XPressTime>0){ Anime=DIVE; AChangeRate=0.08f;}
		
	}
	if(HoldFlag) Anime=STAND_WALK;
}
void CMotion::Dive()
{
	if(PChangeRate<=0.3) PlayTime=0;
	if(PlayTime==1) pPlayerData->Stamina-=100;
	Posture=CREEP;
	GoAngle=oldGoAngle;
	HoldFlag=0;

	if(PlayTime<14) MoveSpeed=pPlayerData->StandSpeed*2.6f;
	else if(14<=PlayTime&&PlayTime<20){
		
		if(MoveSpeed>=0.2) MoveSpeed-=0.8;
		else MoveSpeed=0;
		
	}
	if(MoveSpeed<=0){

		if(Action==NONE){		Anime=CREEP_NONE;	AChangeRate=0.08;}
		else if(Action==WALK){	Anime=CREEP_WALK;	AChangeRate=0.1f;}
		else if(Action==RUN){	Anime=CREEP_RUN;	AChangeRate=0.12f;}
	}
	
}
void CMotion::Cqc1(){}
void CMotion::Cqc2(){}
void CMotion::Cqc3(){}
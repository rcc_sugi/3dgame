#ifndef SYSTEMMGR
#define SYSTEMMGR

#include"GameMgr.h"
#include"Fps.h"

class CSystemMgr{
	
	CGameMgr* pGameMgr;
	Fps* pFps;
public:
	CSystemMgr();
	~CSystemMgr();

	enum eGameState{DEMO,TITLE,SETUP,GAME,};
	eGameState GameState_;
	bool TitleFlag,SetUpFlag,QuitFlag;

	enum eTitleState{START,CONTINUE,SETTING,QUIT,LAST};
	unsigned int iTitleState_;

	bool Initialize();
	void Finalize();
	void MainLoop();
	void Title();
	void SetUp();

	int PreviewHandle;
	unsigned int ScreenSize;
	int PressUpTime,PressDownTime,PressCircleTime,PressXTime;
};


#endif
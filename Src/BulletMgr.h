#ifndef BULLETMGR
#define BULLETMGR

#include"Motion.h"
#include"CameraMgr.h"

class CBullet{

public:

	VECTOR Position;

	bool ExistFlag;
	int Speed;


};

class CBulletMgr{

public:
	CBulletMgr();

	//CBullet Bullet[16];
	void Process(CMotion*,CCameraMgr*);
	void Draw();

	int R1PressTime;
	int BulletNum;

};

#endif 
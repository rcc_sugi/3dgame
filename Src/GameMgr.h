#ifndef GAMEMGR
#define GAMEMGR

#include"CharaMgr.h"
#include"Stage.h"
#include"CollisionMgr.h"
#include"Option.h"

class CGameMgr{

public:
	CCharaMgr*		pCharaMgr;
	CStage*			pStage;
	CCollisionMgr*	pCollisionMgr;
	COption*		pOption;

	CGameMgr();
	~CGameMgr();

	bool Process();
	void Draw();

	void Init();


};


#endif
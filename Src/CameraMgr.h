#ifndef CAMERAMGR
#define CAMERAMGR

class CCameraMgr{

public:
	CCameraMgr();
	~CCameraMgr();

	enum eCameraSwitch{LEFT=-1,CENTER,RIGHT};
	eCameraSwitch CameraSwitch_;
	float ChangeRate;

	void Process(VECTOR,float);
	void Draw();

	VECTOR CenterVector,CameraVector;

	float CameraDistance;
	float CameraSpeed,CameraAccel;
	float AngleH,HSpeed;				//�����p�x
	float AngleV,VSpeed;				//�����p�x

	unsigned int CenterH,CenterV,CircleRange;

};


#endif
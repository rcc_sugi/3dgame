#include"DxLib.h"
#include"JoyPad.h"

CJoyPad::CJoyPad()
{
	DrawFormatString(0,120,GetColor(0,255,0),"joypad_complete");
	ScreenFlip();

	InputX=0;	InputY=0;
	VInputX=0;	VInputY=0;
	dInputX=0;	dInputY=0;

}
void CJoyPad::Process()
{
	// パッド１の入力を取得
	GetJoypadAnalogInput( &InputX , &InputY , DX_INPUT_KEY_PAD1 ) ;

	dInputX=InputX;	dInputY=InputY;

	if(InputX!=0||InputY!=0){
		VInputX=dInputX/VSize(VGet((float)InputX,(float)InputY,0.f));
		VInputY=dInputY/VSize(VGet((float)InputX,(float)InputY,0.f));
	}
	else{
		VInputX=0;	VInputY=0;
	}

	oldVInputX=VInputX;
	oldVInputY=VInputY;
}
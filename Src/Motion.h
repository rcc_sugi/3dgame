#ifndef MOTION
#define MOTION

#include"JoyPad.h"
#include"PlayerData.h"
#include"CameraMgr.h"

class CMotion{

public:
	CMotion();
	~CMotion();

	CJoyPad* pJoyPad;
	CPlayerData* pPlayerData;

	enum eAnime{STAND_NONE,STAND_WALK,STAND_RUN,CROUCH_NONE,CROUCH_WALK,CROUCH_RUN,CREEP_NONE,CREEP_WALK,CREEP_RUN
		,DASH,DIVE,CQC1,CQC2,CQC3,HOLD};

	enum ePosture{STAND,CROUCH,CREEP};
	enum eAction{NONE=4,WALK,RUN};
	enum eState{NORMAL=13,HITTING,DEAD,RETURNING};

	void Process(CCameraMgr*,CPlayerData*);

	ePosture Posture,oldPosture,tempPosture;
	eAction oldAction,Action;

	float PChangeRate,AChangeRate,playAnim,PlayTime,TotalTime;
	int AttachIndex[32],Anime,oldAnime,tempAnime;

	int XPressTime,oldXPressTime,SquarePressTime,oldSquarePressTime,R1PressTime,oldR1PressTime;	
	int DiveTime,ComboTime;
	int MoveRange;
	float AngleP,GoAngle,oldGoAngle;
	float MoveSpeed;
	VECTOR MoveVector,Position,oldPosition;
	float x,y,z;
	float StandSpeed,CrouchSpeed,CreepSpeed;
	float PostureSpeed;
	bool HoldFlag;

	void MotionMG();

	void StandNone();	void StandWalk();	void StandRun();
	void CrouchNone();	void CrouchWalk();	void CrouchRun();
	void CreepNone();	void CreepWalk();	void CreepRun();
	void Dash();	void Dive();
	void Cqc1();	void Cqc2();	void Cqc3();
	void Hold();

};


#endif
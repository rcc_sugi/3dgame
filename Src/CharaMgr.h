#ifndef CHARAMGR
#define CHARAMGR

#include"Player.h"
#include"EnemyMgr.h"

class CCharaMgr{

public:
	CPlayer* pPlayer;
	CEnemyMgr* pEnemyMgr;


	CCharaMgr();
	~CCharaMgr();
	
	void Process();
	void Draw();

};


#endif
#ifndef BACKGROUND
#define BACKGROUND

class CBackground{

public:

	CBackground::CBackground();
	CBackground::~CBackground();

	void Process();
	void Draw();

};

#endif
#include"DxLib.h"
#include"Option.h"


COption::COption(){
	
	StartTime=0;	OptionFlag=0;
	OptionMode=Return;
	UpTime=0;	DownTime=0;
	InitFlag=0;	EndFlag=0;
}
COption::~COption(){}

void COption::Process(){

	if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_12)||CheckHitKey(KEY_INPUT_SPACE)){

		StartTime++;
		if(StartTime==1){
			if(OptionFlag==0) OptionFlag=1;
			else if(OptionFlag==1) OptionFlag=0;
		}
	}
	else StartTime=0;

	if(OptionFlag){
		if(CheckHitKey(KEY_INPUT_DOWN)
			||(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_16)
			)	DownTime++;
		else DownTime=0;

		if(CheckHitKey(KEY_INPUT_UP)
			||(GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_10)
			)	UpTime++;
		else UpTime=0;

		switch (OptionMode){
		case(Return):
			if(DownTime==1) OptionMode=Init;
			if(UpTime==1) OptionMode=End;

			if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_2)||CheckHitKey(KEY_INPUT_RETURN)) OptionFlag=0;
			
			break;
		case(Init):

			if(DownTime==1) OptionMode=End;
			if(UpTime==1) OptionMode=Return;

			if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_2)){

				OptionFlag=0;
				InitFlag=1;
			}

			break;
		case(End):

			if(DownTime==1) OptionMode=Return;
			if(UpTime==1) OptionMode=Init;

			if((GetJoypadInputState( DX_INPUT_PAD1 ) & PAD_INPUT_2)){

				OptionFlag=0;
				EndFlag=1;
			}

			break;
		}

	}
	else OptionMode = Return;
	SelectPosition = CenterV + 15*OptionMode;
			

}
void COption::Draw()
{

	if(OptionFlag){

		DrawFormatString(CenterH-40,CenterV,GetColor(0,0,0),"つづける");
		DrawFormatString(CenterH-40,CenterV+15,GetColor(0,0,0),"はじめから");
		DrawFormatString(CenterH-40,CenterV+30,GetColor(0,0,0),"おわる");
		DrawFormatString(CenterH-55,SelectPosition,GetColor(0,0,0),"->",SelectPosition);

	}

}
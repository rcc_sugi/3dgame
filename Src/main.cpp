#include <DxLib.h>
#include "SystemMgr.h"

// -------------------
// メイン関数
// -------------------
int WINAPI WinMain(HINSTANCE,HINSTANCE,LPSTR,int){
	

	CSystemMgr* pSystemMgr=new CSystemMgr();

	pSystemMgr->MainLoop();
	delete pSystemMgr;
	
	DxLib_End();
	return 0;
} 
#ifndef MOVEMGR
#define MOVEMGR

#include<math.h>
#include"JoyPad.h"
#include"CameraMgr.h"

class CMoveMgr{

public:
	CMoveMgr();
	~CMoveMgr();

	CJoyPad* pJoyPad;

	void Process(CCameraMgr*);


	float AngleP,GoAngle,oldGoAngle;

	VECTOR Position,oldPosition,MoveVector;

	int MoveRange;



};

#endif
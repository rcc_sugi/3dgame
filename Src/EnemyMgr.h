#ifndef ENEMYMGR
#define ENEMYMGR

#include"Player.h"

class CEnemyMgr{

public:
	CEnemyMgr();
	~CEnemyMgr();

	void Init();
	void Process(CPlayer*);
	void Draw();

	int EnemyHandle,EnemyNumber;
	int AttachIndex[32];
	float TotalTime,PlayTime;

	VECTOR Position,Spot1,VectorPE;
	float Rotate,AnglePE,AngleE;

	int HP;
	float Speed;
	float EyeRange,EyeDistance;
	float EarRange;
	float DistancePE;
};




#endif ENEMYMGR
#include"DxLib.h"
#include"Player.h"
#include<math.h>

CPlayer::CPlayer()
{
	DrawFormatString(0,60,GetColor(0,255,0),"player_complete");
	ScreenFlip();

	pPlayerData=new CPlayerData();
	pMotion=	new CMotion();
	pCameraMgr=	new CCameraMgr();

	PCharacter=MIKU;
	SelectCharaFlag=0;

	
};

CPlayer::~CPlayer(){

	delete pPlayerData;
	delete pCameraMgr;
	delete pMotion;

};

void CPlayer::Init()
{

	switch(PCharacter){
	case(MIKU):		pPlayerData->MikuInit();	break;
	case(REIMU):	pPlayerData->ReimuInit();	break;
/*	case(RIN):		pPlayerData->MohikanInit();	break;
	case(REN):;break;
	case(TACHIKOMA):

		PHandle = MV1LoadModel( "dat/タチコマ/Model/タチコマ_MV2T103.pmd" );
		break;
	case(METALGEARREX):

		PHandle = MV1LoadModel( "dat/メタルギア/メタルギアREX小.pmd" );
		break;
		*/
	}

}
void CPlayer::Process()
{
	
	pMotion->Process(pCameraMgr,pPlayerData);

	//カメラがこの位置でないと不自然になる
	pCameraMgr->Process(pMotion->Position,pPlayerData->Height);

}

void CPlayer::Draw()
{

	MV1DrawModel( pPlayerData->PHandle ) ;
	pCameraMgr->Draw();

	/*
	DrawFormatString(40,45,GetColor(0,255,0),"movespeed: %f",pMotion->MoveSpeed);
	DrawFormatString(40,60,GetColor(0,255,0),"playtime: %f",pMotion->PlayTime);
	DrawFormatString(40,75,GetColor(0,255,0),"totaltime: %f",pMotion->TotalTime);
	DrawFormatString(40,90,GetColor(0,255,0),"totaltime/2: %f",pMotion->TotalTime/2);
	DrawFormatString(40,105,GetColor(0,255,0),"chagerate: %f",pMotion->PChangeRate);
	DrawFormatString(40,120,GetColor(0,255,0),"anime: %d",pMotion->Anime);
	DrawFormatString(40,135,GetColor(0,255,0),"onground: %d",pPlayerData->OnGroundFlag);
	DrawFormatString(40,150,GetColor(0,255,0),"stamina: %d",pPlayerData->Stamina);

	DrawFormatString(40,300,GetColor(0,255,0),"x: %f",pMotion->Position.x);
	DrawFormatString(40,315,GetColor(0,255,0),"y: %f",pMotion->Position.y);
	DrawFormatString(40,330,GetColor(0,255,0),"z: %f",pMotion->Position.z);
	*/

}


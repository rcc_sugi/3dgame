#ifndef PLAYERDATA
#define PLAYERDATA


class CPlayerData{

public:
	CPlayerData();

	int PHandle;

	int HPMax,HP;
	int StaminaMax,Stamina;
	int Stealth;
	int Silence;
	int RecoilControl;
	float StandHeight,CrouchHeight,CreepHeight,Height,Weight,Width;
	float StandSpeed,CrouchSpeed,CreepSpeed;
	
	bool OnGroundFlag;
	void MikuInit();
	void ReimuInit();
	void MohikanInit();


};

#endif